using NUnit.Framework;
using TRPOLab3.Lib;

namespace TRPOLab3.Tests
{
    public class Tests
    {
        [Test]
        public void TestAreaSideSurfaceRegularTruncatedPyramid()
        {
            double P = 4;
            double R = 6;
            double h = 3;
            var expected = 15;
            var S = TRPOLab3.Lib.Formula.AreaSideSurfaceRegularTruncatedPyramid(P, R, h);
            Assert.AreEqual(expected, S);
        }

        [Test]
        public void Testnull()
        {
            double R = 6;
            double P = 4;
            double h = 3;
            double S = TRPOLab3.Lib.Formula.AreaSideSurfaceRegularTruncatedPyramid(P, R, h);
            if (S <= 0)
            {
                Assert.Fail("������: ������� �� ����� ���� ������ ��� ����� ����");
            }
            else
            {
                Assert.Pass("���� ������� �������");
            }
        }
    }
}

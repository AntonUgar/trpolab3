﻿using Microsoft.AspNetCore.Mvc;
using System;
using TRPOLab3.Lib;


namespace TRPO_Lab5.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string P, string R, string h)
        {
            double S = Formula.AreaSideSurfaceRegularTruncatedPyramid(Convert.ToDouble(P), Convert.ToDouble(R), Convert.ToDouble(h));
            ViewBag.result = S;
            return View();
        }


    }
}
﻿using System;

namespace TRPOLab3.Lib
{
    public class Formula
    {
        public static double AreaSideSurfaceRegularTruncatedPyramid(double P, double R, double h)
        {
            double S = ((P + R) / 2) * h; // P и R - периметры оснований, h - апофема
            return S;
        }

    }
}

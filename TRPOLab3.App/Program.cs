﻿using System;
using TRPOLab3.Lib;

namespace TRPOLab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите периметр верхнего основания пирамиды");
            string StringP = Console.ReadLine();
            double P = Convert.ToDouble(StringP);

            Console.WriteLine("Введите периметр нижнего основания пирамиды");
            string StringR = Console.ReadLine();
            double R = Convert.ToDouble(StringR);

            Console.WriteLine("Введите апофему");
            string Stringh = Console.ReadLine();
            double h = Convert.ToDouble(Stringh);

            var S = Formula.AreaSideSurfaceRegularTruncatedPyramid(P, R, h);
            Console.WriteLine($"Площадь боковой поверхности правильной усеченной пирамиды равна {S}");
        }
    }
}

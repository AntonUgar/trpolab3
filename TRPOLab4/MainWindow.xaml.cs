﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPOLab3.Lib;

namespace TRPOLab4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged

    {
        private double P;
        public double P1
        {
            set
            {
                P = value;
                OnPropertyChanged(nameof(P1));
                OnPropertyChanged(nameof(S));
            }
            get => P;
        }
        private double R;
        public double R1
        {
            set
            {
                R = value;
                OnPropertyChanged(nameof(R1));
                OnPropertyChanged(nameof(S));
            }
            get => R;

        }
        private double h;
        public double h1
        {
            set
            {
                h = value;
                OnPropertyChanged(nameof(h1));
                OnPropertyChanged(nameof(S));
            }
            get => h;
        }

        public double S => Formula.AreaSideSurfaceRegularTruncatedPyramid(P, R, h);
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }


}

